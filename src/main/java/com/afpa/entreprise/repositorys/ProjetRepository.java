package com.afpa.entreprise.repositorys;

import com.afpa.entreprise.models.ProjetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjetRepository extends JpaRepository<ProjetEntity,Long> {
}

package com.afpa.entreprise.repositorys;

import com.afpa.entreprise.models.SalarieEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalarieRepository extends JpaRepository<SalarieEntity,Long> {
}

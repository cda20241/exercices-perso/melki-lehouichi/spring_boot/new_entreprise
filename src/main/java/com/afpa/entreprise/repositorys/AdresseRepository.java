package com.afpa.entreprise.repositorys;

import com.afpa.entreprise.models.AdresseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<AdresseEntity,Long> {
}

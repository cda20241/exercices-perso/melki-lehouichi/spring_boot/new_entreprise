package com.afpa.entreprise.repositorys;

import com.afpa.entreprise.models.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<ServiceEntity,Long> {
}

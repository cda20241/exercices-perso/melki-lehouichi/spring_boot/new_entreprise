package com.afpa.entreprise.controllers;

import com.afpa.entreprise.models.AdresseEntity;
import com.afpa.entreprise.models.ProjetEntity;
import com.afpa.entreprise.services.AdresseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/adresse")
public class AdresseController {
    @Autowired
    private final AdresseService adresseService;

    public AdresseController(AdresseService adresseService) {
        this.adresseService = adresseService;
    }

    @GetMapping("/all")
    public List<AdresseEntity> getAllAdresses(){return this.adresseService.getAllAdresses();}

    @GetMapping("/{id}")
    public AdresseEntity getAdresseById(@PathVariable Long id){return this.adresseService.getAdresse(id);}

    @PostMapping("/add")
    public ResponseEntity<String> addAdresse(@RequestBody AdresseEntity adresse){
        return this.adresseService.createAdresse(adresse);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateAdresse(@PathVariable Long id, @RequestBody AdresseEntity adresse){
        return this.adresseService.updateAdresse(id,adresse);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> updateAdresse(@PathVariable Long id){
        return this.adresseService.deleAdresse(id);
    }
}

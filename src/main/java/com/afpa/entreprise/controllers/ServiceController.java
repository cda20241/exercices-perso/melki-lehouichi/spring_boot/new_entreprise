package com.afpa.entreprise.controllers;

import com.afpa.entreprise.models.AdresseEntity;
import com.afpa.entreprise.models.ServiceEntity;
import com.afpa.entreprise.services.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/service")
public class ServiceController {
    @Autowired
    private final ServiceService serviceService;

    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @GetMapping("/all")
    public List<ServiceEntity> getAllServices(){return this.serviceService.getAllServices();}

    @GetMapping("/{id}")
    public ServiceEntity getServiceById(@PathVariable Long id) {return this.serviceService.getService(id);}

    @PostMapping("/add")
    public ResponseEntity<String> addService(@RequestBody ServiceEntity service){
        return this.serviceService.createService(service);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateService(@PathVariable Long id, @RequestBody ServiceEntity service){
        return this.serviceService.updateService(id,service);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> updateService(@PathVariable Long id){
        return this.serviceService.deleService(id);
    }
}

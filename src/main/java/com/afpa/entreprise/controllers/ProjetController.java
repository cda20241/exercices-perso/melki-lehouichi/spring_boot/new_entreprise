package com.afpa.entreprise.controllers;

import com.afpa.entreprise.models.ProjetEntity;
import com.afpa.entreprise.services.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projet")
public class ProjetController {
    @Autowired
    private final ProjetService projetService;

    public ProjetController(ProjetService projetService) {
        this.projetService = projetService;
    }

    @GetMapping("/all")
    public List<ProjetEntity> getAllProjets(){return this.projetService.getAllProjets();}

    @GetMapping("/{id}")
    public ProjetEntity getProjetById(@PathVariable Long id){return this.projetService.getProjet(id);}

    @PostMapping("/add")
    public ResponseEntity<String> addProject(@RequestBody ProjetEntity projet){
        return this.projetService.createProjet(projet);
    }

    @PostMapping("/add_salarie/{idProjet}/{idSalarie}")
    public ResponseEntity<String> addSalarieToProjet(@PathVariable Long idProjet,@PathVariable Long idSalarie){
        return this.projetService.addSalarieToProjet(idProjet,idSalarie);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateProject(@PathVariable Long id, @RequestBody ProjetEntity projet){
        return this.projetService.updateProjet(id,projet);
    }
    @DeleteMapping("/delete/{idProjet}/{idSalarie}")
    public ResponseEntity<String> deleteSalarieFromProjet(@PathVariable Long idProjet,@PathVariable Long idSalarie){
        return this.projetService.removeSalarieFromProjet(idProjet,idSalarie);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> updateProject(@PathVariable Long id){
        return this.projetService.deleteProjet(id);
    }
}

package com.afpa.entreprise.controllers;

import com.afpa.entreprise.models.AdresseEntity;
import com.afpa.entreprise.models.SalarieEntity;
import com.afpa.entreprise.services.SalarieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salarie")
public class SalarieController {
    @Autowired
    public final SalarieService salarieService;

    public SalarieController(SalarieService salarieService) {
        this.salarieService = salarieService;
    }
    @GetMapping("/all")
    public List<SalarieEntity> getAllSalaries(){return this.salarieService.getAllSalaries();}

    @GetMapping("/{id}")
    public SalarieEntity getSalarieById(@PathVariable Long id){return this.salarieService.getSalarie(id);}

    @PostMapping("/add")
    public ResponseEntity<String> addSalarie(@RequestBody SalarieEntity salarie){
        return this.salarieService.createSalarie(salarie);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateSalarie(@PathVariable Long id, @RequestBody SalarieEntity salarie){
        return this.salarieService.updateSalarie(id,salarie);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> updateSalarie(@PathVariable Long id){
        return this.salarieService.deleSalarie(id);
    }

}

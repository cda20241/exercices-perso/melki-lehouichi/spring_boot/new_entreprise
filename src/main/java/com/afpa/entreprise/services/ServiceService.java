package com.afpa.entreprise.services;

import com.afpa.entreprise.models.AdresseEntity;
import com.afpa.entreprise.models.ServiceEntity;
import com.afpa.entreprise.repositorys.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService {
    @Autowired
    private final ServiceRepository serviceRepository;
    public ServiceService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }
    /** ----------------------- CRUD ---------------------------------------------------------*/
    public List<ServiceEntity> getAllServices(){return serviceRepository.findAll();}
    public ServiceEntity getService(Long id){return serviceRepository.findById(id).orElse(null);}
    public ResponseEntity<String> createService(ServiceEntity service){
        service.setIdService(null);
        service = serviceRepository.save(service);
        return new ResponseEntity<>("Le service' " + service.getNomService() + " a été ajouté !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> updateService(Long id,ServiceEntity service){
        service.setIdService(id);
        service = serviceRepository.save(service);
        return new ResponseEntity<>("Le service' " + service.getNomService() + " a été modifié !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> deleService(Long id){
        ServiceEntity service = serviceRepository.findById(id).orElse(null);
        if (service!=null){
            serviceRepository.deleteById(id);
            return new ResponseEntity<>("Le service' " + service.getNomService() +" a été supprimé !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Le service n'existe pas !",HttpStatus.NOT_FOUND);
        }
    }
}

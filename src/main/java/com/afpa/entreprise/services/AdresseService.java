package com.afpa.entreprise.services;

import com.afpa.entreprise.models.AdresseEntity;
import com.afpa.entreprise.repositorys.AdresseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdresseService {
    @Autowired
    private final AdresseRepository adresseRepository;

    public AdresseService(AdresseRepository adresseRepository) {
        this.adresseRepository = adresseRepository;
    }

    /** ----------------------- CRUD ---------------------------------------------------------*/
    public List<AdresseEntity> getAllAdresses(){return adresseRepository.findAll();}
    public AdresseEntity getAdresse(Long id){return adresseRepository.findById(id).orElse(null);}
    public ResponseEntity<String> createAdresse(AdresseEntity adresse){
        adresse.setIdAdresse(null);
        adresse = adresseRepository.save(adresse);
        return new ResponseEntity<>("L'adresse' " + adresse.getRue()+" "+adresse.getVille()+ " a été ajoutée !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> updateAdresse(Long id,AdresseEntity adresse){
        adresse.setIdAdresse(id);
        adresse = adresseRepository.save(adresse);
        return new ResponseEntity<>("L'adresse' " + adresse.getRue()+" "+adresse.getVille()+ " a été modifiée !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> deleAdresse(Long id){
        AdresseEntity adresse = adresseRepository.findById(id).orElse(null);
        if (adresse!=null){
            adresseRepository.deleteById(id);
            return new ResponseEntity<>("L'adresse' " + adresse.getRue()+" "+adresse.getVille()+ " a été supprimée !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>(" Cette adresse n'existe pas !",HttpStatus.NOT_FOUND);
        }
    }
}

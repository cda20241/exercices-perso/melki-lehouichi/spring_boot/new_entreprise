package com.afpa.entreprise.services;

import com.afpa.entreprise.models.SalarieEntity;
import com.afpa.entreprise.repositorys.AdresseRepository;
import com.afpa.entreprise.repositorys.ProjetRepository;
import com.afpa.entreprise.repositorys.SalarieRepository;
import com.afpa.entreprise.repositorys.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SalarieService {
    @Autowired
    private final SalarieRepository salarieRepository;

    @Autowired
    private final AdresseRepository adresseRepository;

    @Autowired
    private final ServiceRepository serviceRepository;

    @Autowired
    private final ProjetRepository projetRepository;
    public SalarieService(SalarieRepository salarieRepository,AdresseRepository adresseRepository, ServiceRepository serviceRepository, ProjetRepository projetRepository) {
        this.salarieRepository = salarieRepository;
        this.adresseRepository=adresseRepository;
        this.serviceRepository=serviceRepository;
        this.projetRepository=projetRepository;
    }
    /** ----------------------- CRUD ---------------------------------------------------------*/
    public List<SalarieEntity> getAllSalaries(){return salarieRepository.findAll();}
    public SalarieEntity getSalarie(Long id){return salarieRepository.findById(id).orElse(null);}
    public ResponseEntity<String> createSalarie(SalarieEntity salarie){
        salarie.setIdSalarie(null);
        salarie = salarieRepository.save(salarie);
        return new ResponseEntity<>("Le service' " + salarie.getNomSalarie() + " a été ajouté !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> updateSalarie(Long id,SalarieEntity salarie){
        salarie.setIdSalarie(id);
        salarie = salarieRepository.save(salarie);
        return new ResponseEntity<>("Le service' " + salarie.getNomSalarie() + " a été modifié !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> deleSalarie(Long id){
        SalarieEntity salarie = salarieRepository.findById(id).orElse(null);
        if (salarie!=null){
            salarieRepository.deleteById(id);
            return new ResponseEntity<>("Le service' " + salarie.getNomSalarie() +" a été supprimé !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Le service n'existe pas !",HttpStatus.NOT_FOUND);
        }
    }
}

package com.afpa.entreprise.services;

import com.afpa.entreprise.models.ProjetEntity;
import com.afpa.entreprise.models.SalarieEntity;
import com.afpa.entreprise.repositorys.ProjetRepository;
import com.afpa.entreprise.repositorys.SalarieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProjetService {
    @Autowired
    private final ProjetRepository projetRepository;
    @Autowired
    private final SalarieRepository salarieRepository;

    public ProjetService(ProjetRepository projetRepository,SalarieRepository salarieRepository) {
        this.projetRepository = projetRepository;
        this.salarieRepository=salarieRepository;
    }
    /** ----------------------- CRUD ---------------------------------------------------------*/
    public List<ProjetEntity> getAllProjets(){return projetRepository.findAll();}

    public ProjetEntity getProjet(Long id){return projetRepository.findById(id).orElse(null);}
    public ResponseEntity<String> createProjet(ProjetEntity projet){
        projet.setIdProjet(null);
        projet = projetRepository.save(projet);
        return new ResponseEntity<>("Le projet' " + projet.getNomProjet() + " a été ajouté !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> addSalarieToProjet(Long projetId, Long salarieId) {
        Optional<ProjetEntity> optionalProjet = projetRepository.findById(projetId);
        Optional<SalarieEntity> optionalSalarie = salarieRepository.findById(salarieId);

        if (optionalProjet.isPresent() && optionalSalarie.isPresent()) {
            ProjetEntity projet = optionalProjet.get();
            SalarieEntity salarie = optionalSalarie.get();

            if (projet.getSalaries().contains(salarie)) {
                return new ResponseEntity<>("Le salarié fait déjà partie de ce projet !",HttpStatus.NOT_FOUND);
            }
            projet.getSalaries().add(salarie);
            projetRepository.save(projet);
            return new ResponseEntity<>("Le salarié a été ajouté au projet avec succès !", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Projet ou salarié non trouvé !",HttpStatus.NOT_FOUND);
        }
    }
    public ResponseEntity<String> removeSalarieFromProjet(Long projetId, Long salarieId) {
        Optional<ProjetEntity> optionalProjet = projetRepository.findById(projetId);
        Optional<SalarieEntity> optionalSalarie = salarieRepository.findById(salarieId);

        if (optionalProjet.isPresent() && optionalSalarie.isPresent()) {
            ProjetEntity projet = optionalProjet.get();
            SalarieEntity salarie = optionalSalarie.get();

            if (!projet.getSalaries().contains(salarie)) {
                return new ResponseEntity<>("Le salarié fait déjà partie de ce projet !",HttpStatus.NOT_FOUND);
            }

            projet.getSalaries().remove(salarie);
            projetRepository.save(projet);
            return new ResponseEntity<>("Le projet' " + projet.getNomProjet() +" a été supprimé !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Projet ou salarié non trouvé !",HttpStatus.NOT_FOUND);
        }
    }
    public ResponseEntity<String> updateProjet(Long id,ProjetEntity projet){
        projet.setIdProjet(id);
        projet = projetRepository.save(projet);
        return new ResponseEntity<>("Le projet' " + projet.getNomProjet() + " a été modifié !", HttpStatus.CREATED);
    }
    public ResponseEntity<String> deleteProjet(Long id){
        ProjetEntity projet = projetRepository.findById(id).orElse(null);
        if (projet!=null){
            projetRepository.deleteById(id);
            return new ResponseEntity<>("Le projet' " + projet.getNomProjet() +" a été supprimé !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Le projet n'existe pas !",HttpStatus.NOT_FOUND);
        }
    }
}

package com.afpa.entreprise.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "salarie")
public class SalarieEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSalarie",nullable = false,updatable = false)
    private Long idSalarie;

    @Column(name = "nomSalarie")
    private String nomSalarie;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idAdresse")
    private AdresseEntity adresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idService")
    private ServiceEntity service;

    @ManyToMany(mappedBy = "salaries")
    @JsonIgnore
    private Set<ProjetEntity> projets = new HashSet<>();


    /**-------------- CONSTRUCTEURS -------------------*/
    public SalarieEntity() {
    }

    public SalarieEntity(Long idSalarie, String nomSalarie, AdresseEntity adresse, ServiceEntity service) {
        this.idSalarie = idSalarie;
        this.nomSalarie = nomSalarie;
        this.adresse = adresse;
        this.service = service;
    }

    public SalarieEntity(String nomSalarie) {
        this.nomSalarie = nomSalarie;
    }
    /**-------------- GETTER & SETTER -------------------*/
    public Long getIdSalarie() {
        return idSalarie;
    }

    public void setIdSalarie(Long idSalarie) {
        this.idSalarie = idSalarie;
    }

    public String getNomSalarie() {
        return nomSalarie;
    }

    public void setNomSalarie(String nomSalarie) {
        this.nomSalarie = nomSalarie;
    }

    public AdresseEntity getAdresse() {
        return adresse;
    }

    public void setAdresse(AdresseEntity adresse) {
        this.adresse = adresse;
    }

    public ServiceEntity getService() {
        return service;
    }

    public void setService(ServiceEntity service) {
        this.service = service;
    }

    public Set<ProjetEntity> getProjets() {
        return projets;
    }

    public void setProjets(Set<ProjetEntity> projets) {
        this.projets = projets;
    }
}

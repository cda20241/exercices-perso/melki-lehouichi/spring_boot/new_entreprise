package com.afpa.entreprise.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "service")
public class ServiceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idService",nullable = false,updatable = false)
    private Long idService;

    @Column(name = "nomService")
    private String nomService;


    /**-------------- CONSTRUCTEURS -------------------*/
    public ServiceEntity() {
    }

    public ServiceEntity(Long idService, String nomService) {
        this.idService = idService;
        this.nomService = nomService;
    }

    public ServiceEntity(String nomService) {
        this.nomService = nomService;
    }
    /**-------------- GETTER & SETTER -------------------*/

    public Long getIdService() {
        return idService;
    }

    public void setIdService(Long idService) {
        this.idService = idService;
    }

    public String getNomService() {
        return nomService;
    }

    public void setNomService(String nomService) {
        this.nomService = nomService;
    }

}

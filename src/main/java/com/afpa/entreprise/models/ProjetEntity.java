package com.afpa.entreprise.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "projet")
public class ProjetEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProjet",nullable = false,updatable = false)
    private Long idProjet;

    @Column(name = "nomProjet")
    private String nomProjet;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "salarieProject",
            joinColumns = @JoinColumn(name = "idProjet"),
            inverseJoinColumns = @JoinColumn(name = "idSalarie")
    )
    private Set<SalarieEntity> salaries = new HashSet<>();

    /**-------------- CONSTRUCTEURS -------------------*/
    public ProjetEntity() {
    }

    public ProjetEntity(Long idProjet, String nomProjet) {
        this.idProjet = idProjet;
        this.nomProjet = nomProjet;
    }

    public ProjetEntity(String nomProjet) {
        this.nomProjet = nomProjet;
    }
    /**-------------- GETTER & SETTER -------------------*/
    public Long getIdProjet() {
        return idProjet;
    }

    public void setIdProjet(Long idProjet) {
        this.idProjet = idProjet;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public Set<SalarieEntity> getSalaries() {
        return salaries;
    }

    public void setSalaries(Set<SalarieEntity> salaries) {
        this.salaries = salaries;
    }
}
